# LSP Server for WebHare

> NOTE: This module is deprecated because its functionality is now available through the
> [WebHare Dev module](https://gitlab.com/webhare/dev). After updating the Dev module to at least version 1.2.0, you should
> remove this module from your WebHare installation (using `wh module rm lsp`).

Experimental [Language Server Protocol](https://microsoft.github.io/language-server-protocol/specifications/specification-current/)
implementation for [WebHare](https://gitlab.com/webhare/platform).


## Features

The following LSP features are supported by the WebHare language server:

* File diagnostics for HareScript and Witty files and supported XML files (e.g. module definitions, WRD schemas, screens, site profiles)
* Code actions to automatically add missing LOADLIBs and remove unused LOADLIBs
* Documentation popups on hover
* Jump to definition


## Installation and usage

The WebHare language server is supported in the following applications:

* Sublime Text, through the [`LSP-webhare`](https://gitlab.com/webhare/lsp/LSP-webhare) package
* Visual Studio Code, through the [`webhare-language-vscode`](https://gitlab.com/webhare/lsp/webhare-language-vscode) extension
* Nova, through the [`WebHare`](https://extensions.panic.com/extensions/dev.webhare/dev.webhare.WebHare/) extension

See those repositories for specific installation and configuration instructions.


## How it works

* The language clients use the `@webhare/language-server` language server, which communicates with the WebHare `lsp` module.
* When LSP functionality is requested for a document, the language server is started (the main script `server.ts` is run).
* The server sets up a connection to the editor (in `connection.ts`) which receives the application capabilities and configuration and sends the language server capabilities back.
* The language server actions are invoked through events on the connection object (e.g. `connection.onDefinition`), which in turn call the appropriate functions in `service.ts` (e.g. `definitionRequest`).
* The first thing a service function does, is establishing a WebHareConnection in `whconnect.ts`. This will search for the connection info by finding the most relevant `.wh.connectinfo` file, which contains the server address and access token. These are used to setup a connection to the WebSocket defined by the `lsp` module.
* Connections are cached using the `.wh.connectinfo` file's directory, so for subsequent calls for documents within that directory, the connection can be reused.
* The functions in `service.ts` call the `sendRequest` (expecting a result) or `sendNotification` (no result expected) function of the WebHareConnection.
* `lsp.whsock` receives the socket message and calls `CallLSPMethod`, which dynamically calls the requested function in `service.whlib` (prefixed with `LSP_`).
* The module uses the `HareScriptFile` object as an abstraction for a HareScript file, which has the actual implementation of the different file actions.
* Code action requests return commands, which are registered through the server capabilities. When a command is run by the editor, the requested command is run by dynamically calling the function that implements the command in `service.whlib` (prefixed with `CMD_`). The first argument of a called command is always the text document uri, followed by the arguments returned with the code action.
* The language server also defines custom actions, which are prefixed with `webHare/`, for example `webHare/getStackTrace`. These are defined in `protocol.ts`. The exported definitions of `protocol.ts` can be used in language clients that are written in TypeScript.
